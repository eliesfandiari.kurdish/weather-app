////keys////
autocompleteKey = "f18673af25bc45379cfdf71d63734b33";
hourlyKey = "8e0dc206b92d34a2e1fb8433bd58d226"

////variables///
let result = document.getElementById("result")
let result2 = document.getElementById("result-2")
let resultAutocomplete = document.getElementById("result-autocomplete")
let inputElem = document.getElementById("city")

////functions////
function setQuery(e) {
    if (e.keyCode === 13) {
        getWeather(inputElem.value)
        inputElem.value = ""
        inputElem.style.borderRadius = "20px"
        resultAutocomplete.style.display = "none"
    }
}

let getWeather = () => {
    let cityValue = inputElem.value;
    let url = `https://api.openweathermap.org/data/2.5/weather?q=${cityValue}&appid=${hourlyKey}&units=metric`
    fetch(url).then((resp) => resp.json()).then((data) => {
        console.log(data)
        result.innerHTML = `
            <h2>${data.name}</h2>
            <h4 class="description">${data.weather[0].description}</h4>
            <h1>${data.main.temp}&#176;C</h1>
            <div class="pressure-humidity">
                <h4>Pressure: ${data.main.pressure}</h4>
                <h4>Humidity: ${data.main.humidity}</h4>
            </div> 
            `
        fetch(`https://api.openweathermap.org/data/2.5/forecast?lat=${data.coord.lat}&lon=${data.coord.lon}&appid=${hourlyKey}&units=metric`)
            .then(response => response.json())
            .then(hour => {
                result2.innerHTML = `
                    <div class="container-hour">
                        <div class="hour">
                            <h4>${hour.list[0].dt_txt}</h4>
                            <img src="https://openweathermap.org/img/w/${hour.list[0].weather[0].icon}.png" alt="">
                            <h4>${hour.list[0].main.temp}&#176;C</h4>
                        </div>
                        <div class="hour">
                            <h4>${hour.list[1].dt_txt}</h4>
                            <img src="https://openweathermap.org/img/w/${hour.list[1].weather[0].icon}.png" alt="">
                            <h4>${hour.list[1].main.temp}&#176;C</h4>
                        </div>
                        <div class="hour">
                            <h4>${hour.list[2].dt_txt}</h4>
                            <img src="https://openweathermap.org/img/w/${hour.list[2].weather[0].icon}.png" alt="">
                            <h4>${hour.list[2].main.temp}&#176;C</h4>
                        </div>
                        <div class="hour">
                            <h4>${hour.list[3].dt_txt}</h4>
                            <img src="https://openweathermap.org/img/w/${hour.list[3].weather[0].icon}.png" alt="">
                            <h4>${hour.list[3].main.temp}&#176;C</h4>
                        </div>
                        <div class="hour">
                            <h4>${hour.list[4].dt_txt}</h4>
                            <img src="https://openweathermap.org/img/w/${hour.list[4].weather[0].icon}.png" alt="">
                            <h4>${hour.list[4].main.temp}&#176;C</h4>
                        </div>
                    </div>
                    <div class="container-day">
                        <div class="day">
                            <h3>Next Morning</h3>
                            <div>
                                <h2>${hour.list[7].main.temp}&#176;C</h2>
                                <img src="https://openweathermap.org/img/w/${hour.list[7].weather[0].icon}.png" alt="">
                            </div>  
                        </div>
                        <div class="day">
                            <h3>2 Days Later</h3>
                            <div>
                                <h2>${hour.list[15].main.temp}&#176;C</h2>
                                <img src="https://openweathermap.org/img/w/${hour.list[15].weather[0].icon}.png" alt="">
                            </div>  
                         </div>
                         <div class="day">
                            <h3>3 Days Later</h3>
                                <div>
                                    <h2>${hour.list[23].main.temp}&#176;C</h2>
                                    <img src="https://openweathermap.org/img/w/${hour.list[23].weather[0].icon}.png" alt="">
                                </div>  
                           </div>
                        </div>
                    `
                console.log(hour)
            })
    })
        .catch(() => {
            result.innerHTML = ``
            result2.innerHTML = ``
        })
}

inputElem.addEventListener("keypress", setQuery)
window.addEventListener("load", getWeather)

//////////////////////////////////////////////

function setValue (e){
    let listElemAutocomplete = document.querySelectorAll(".autocomplete-list-item")
    listElemAutocomplete.forEach(function (word) {
        word.addEventListener("click", function (event) {
            inputElem.value = event.target.textContent
            console.log(event)
            inputElem.focus()
        })
    })
}

let autocompleteFunc = () => {
    let cityValue = inputElem.value;
    fetch(`https://api.geoapify.com/v1/geocode/autocomplete?text=${cityValue}&type=city&format=json&apiKey=${autocompleteKey}`)
        .then(response => response.json())
        .then(result => {
            let resultArray = result.results
            console.log(resultArray)
            for(let value of resultArray){
                console.log(value.city)
                if (value.city === undefined){
                    console.log(3)
                    value.city = ""
                }
            }
            if (cityValue.length > 2) {
                resultAutocomplete.style.display = "flex"
                resultAutocomplete.style.flexDirection = "column"
                resultAutocomplete.style.justifyContent = "flex-start"
                inputElem.style.borderRadius = "5px"
                resultAutocomplete.innerHTML = `
                <li class="autocomplete-list-item"><a>${resultArray[0].city}</a></li>
                <li class="autocomplete-list-item"><a>${resultArray[1].city}</a></li>
                <li class="autocomplete-list-item"><a>${resultArray[2].city}</a></li>
                <li class="autocomplete-list-item"><a>${resultArray[3].city}</a></li>
                `
                if (cityValue.length > 3) {
                    resultAutocomplete.innerHTML = `
                <li class="autocomplete-list-item"><a>${resultArray[0].city}</a></li>
                <li class="autocomplete-list-item"><a>${resultArray[1].city}</a></li>
                <li class="autocomplete-list-item"><a>${resultArray[2].city}</a></li>
                `
                    if (cityValue.length > 4) {
                        resultAutocomplete.innerHTML = `
                <li class="autocomplete-list-item"><a>${resultArray[0].city}</a></li>
                <li class="autocomplete-list-item"><a>${resultArray[1].city}</a></li>
                
                        `
                    }
                }
                console.log(result)
                setValue()
                setQuery()

            } else {
                resultAutocomplete.style.display = "none"
                resultAutocomplete.innerHTML = ``
                resultAutocomplete.style.display = "none"

            }
        })
        .catch(error => console.log('error', error));
}
inputElem.addEventListener("keyup", autocompleteFunc)
